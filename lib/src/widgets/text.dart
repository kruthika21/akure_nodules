import 'package:akrue_module/r.g.dart';
import 'package:flutter/material.dart';

class ATW extends StatelessWidget {
  final String text;
  final color;
  final double size;
  final double letterSpacing;
  final FontWeight fontWeight;

  const ATW(
      {Key key,
      @required this.text,
      this.color = Colors.black,
        this.size=20,
      this.letterSpacing = 2,
      this.fontWeight = FontWeight.w400})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontFamily: R.fontFamily.quickSand,
          color: color,
          fontSize: size,
          fontWeight: fontWeight,
          letterSpacing: letterSpacing),
    );
  }
}
