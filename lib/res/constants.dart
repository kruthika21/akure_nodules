import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

const APP_NAME = 'AKRUE';
const TAG_LINE = 'Own Your Brand';
const BASE_URL =
    kReleaseMode ? 'http://157.230.80.207:3000' : 'http://157.230.80.207:3000';

class AppColors {
  static final warningRed = Colors.red[800];
  static final greenAccent700 = Colors.greenAccent[700];
  static final t1 = Colors.black;
  static final t2 = Colors.white;
}

class HeroTags {
  static final collectionsFabHeroTag = '_collectionsFabHeroTag';
  static final productViewFabHeroTag = '_productViewFabHeroTag';
  static final athletePageSetupPag2HeroTag = '_athletePageSetupPag2HeroTag';
}
